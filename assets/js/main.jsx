import React from 'react';
import ReactDOM from 'react-dom';
import ImageSearchApp from './components/ImageSearchApp.jsx';

require("../styles/main.scss");
require("bootstrap-sass");
require("jquery");

ReactDOM.render(
	<ImageSearchApp />, 
	document.getElementById('image-search-app'));