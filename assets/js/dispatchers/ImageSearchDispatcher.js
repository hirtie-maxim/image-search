import { Dispatcher } from 'flux';

class ImageSearchDispatcher extends Dispatcher {
	handleViewAction(action) {
		this.dispatch({
			source: 'VIEW_ACTION',
			action: action
		});
	}
}

export default new ImageSearchDispatcher();