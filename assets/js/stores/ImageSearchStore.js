import ImageSearchDispatcher from '../dispatchers/ImageSearchDispatcher';
import ImageSearchConstants from '../constants/ImageSearchConstants';
import EventEmitter from 'events';
import AR from '../utils/AsyncRequests';

let CHANGE_EVENT = 'change';

class ImageSearchStore {
    static search(imageSearchObject) {
    	return AR.postImage('/image', imageSearchObject);
    }

    static getResults(asyncToken) {
        return AR.get(`/results/${asyncToken}`);
    }

    static emitChange(event, imageSearchObject) {
    	var isString = typeof event === "string" || event instanceof String;
    	ImageSearchStore.emitter.emit(isString ? event : CHANGE_EVENT, isString ? imageSearchObject : event);
    }

    static addChangeListener(event, callback) {
    	var isString = typeof event === "string" || event instanceof String;
        ImageSearchStore.emitter.on(isString ? event : CHANGE_EVENT, isString ? callback : event);
    }

    static removeChangeListener(event, callback) {
    	var isString = typeof event === "string" || event instanceof String;
        ImageSearchStore.emitter.removeListener(isString ? event : CHANGE_EVENT, isString ? callback : event);
    }
}

ImageSearchStore.emitter = new EventEmitter();
ImageSearchStore.dispatchIndex = ImageSearchDispatcher.register((payload) => {
	var action = payload.action;
    console.log(payload);
	switch(action.actionType) {
		case ImageSearchConstants.SEARCH_IMAGE:
			ImageSearchStore.emitChange(ImageSearchConstants.BEFORE_SEARCH_IMAGE, payload.action.item);
			ImageSearchStore.search(payload.action.item).then(function(result) {
                ImageSearchStore.emitChange(ImageSearchConstants.SEARCH_IMAGE, payload.action.item);
                ImageSearchStore.emitChange(ImageSearchConstants.AFTER_SEARCH_IMAGE, result);
            });
            break;
        case ImageSearchConstants.GET_RESULTS:
            ImageSearchStore.getResults(payload.action.item).then(function(result) {
                ImageSearchStore.emitChange(ImageSearchConstants.GET_RESULTS, payload.action.item);
            });
            break;
	}
});

export default ImageSearchStore;