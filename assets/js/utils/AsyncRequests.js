export default class AsyncRequests {
	static get(urlPath) {
		return new Promise((resolve, reject) => {
			$.get(urlPath, function(result) {
				if(result.error) {
					reject();
					return;
				}

				resolve(result);
			});
		});
	}

	static postImage(pathUrl, data) {
		return new Promise((resolve, reject) => {
            var formData = new FormData();
            formData.append('mode', data.mode);
            formData.append('minWidth', data.minWidth);
            formData.append('minHeight', data.minHeight);
            formData.append('url', data.url);
            formData.append('image', data.image);
            $.ajax({
                url: pathUrl,
                type: 'POST',
                success: (result) => {
                	resolve(result);
                },
                error: (err) => {
                	reject(err);
                },
                data: formData,
                contentType: false,
                processData: false
            });
        });
	}
}