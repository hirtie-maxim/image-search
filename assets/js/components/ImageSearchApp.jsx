import React from 'react';
import { Router, Route, IndexRoute } from 'react-router';
import { createHistory, useBasename } from 'history';

import Layout from './Layout.jsx';
import ImageSearchForm from './ImageSearchForm.jsx';
import ImageSearchResult from './ImageSearchResult.jsx';
import HomePage from './HomePage.jsx';

const history = useBasename(createHistory)({
  basename: '/'
});

class ImageSearchApp extends React.Component {
	render() {
		return (
			<Router>
				<Route path="/" component={Layout}>
					<IndexRoute component={HomePage}/>
					<Route path="/search" component={ImageSearchForm}/>
					<Route path="/result/:token" component={ImageSearchResult} />
				</Route>
			</Router>
		);
	}
}

export default ImageSearchApp;