import React from 'react';
import Formsy from 'formsy-react';
import { File, Input, Row } from 'formsy-react-components';
import Switch from 'react-bootstrap-switch';
import Loader from 'react-loader';
import { Link, History } from 'react-router';
import reactMixin from 'react-mixin';

import ImageSearchActions from '../actions/ImageSearchActions';
import ImageSearchStore from '../stores/ImageSearchStore';
import ImageSearchConstants from '../constants/ImageSearchConstants';

import ResponseDataShow from './ResponseDataShow.jsx';
import RequestDataShow from './RequestDataShow.jsx';

require('../../../node_modules/react-bootstrap-switch/dist/css/bootstrap3/react-bootstrap-switch.css');

class ImageSearchForm extends React.Component {
	constructor() {
		super();
	}

	render() {
		var link = this.state.asyncToken ? <Link to={`/result/${this.state.asyncToken}`}>Show results</Link> : <div></div>;

		return (
			<div id="image-search-form">
				<Loader loaded={this.state.loaded}>
					<Formsy.Form role="search" onValidSubmit={this._onSubmit} className="form-horizontal">
						<div className="form-group">
							<Input rowClassName="form-group" className="col-sm-10" placeholder="Url to image" id="url" 
								name="url" type="text" label="Url" validations="isUrl" 
								value={this.state.url} validationError="This is not a valid url" required />
							<File rowClassName="form-group" className="col-sm-10" id="image" 
								name="image" label="File" onChange={this._onImageOpen} />
							<Input rowClassName="form-group" className="col-sm-10" placeholder="Minimal height" id="minHeight" 
								name="minHeight" type="text" label="Min height" validations="isInt"
								value={this.state.minHeight} validationError="The value should be number" />
							<Input rowClassName="form-group" className="col-sm-10" placeholder="Minimal width" id="minWidth" 
								name="minWidth" type="text" label="Min width" validations="isInt"
								value={this.state.minWidth} validationError="The value should be number" />
							<Row label="Result" layout="horizontal" rowClassName="form-group">
								<Switch state={this.state.mode === 'async'} onChange={this._onSwitchChange} onText="Async" offText="Sync" />
            				</Row>
						</div>
      					<div className="form-group">
							<div className="col-sm-offset-2 col-sm-10">
								<button type="submit" className="btn btn-default">Search</button>
							</div>
						</div>
						{link}
					</Formsy.Form>
				</Loader>
				<RequestDataShow />
				<ResponseDataShow />
			</div>
		);
	}

	_onSubmit = (model) => {
		model.image = this.imageFile;
		model.mode = this.state.mode;
		if(model.asyncToken) {
			ImageSearchActions.getResults(asyncToken);
		} else {
			ImageSearchActions.search(model);
		}
	}

	_onImageOpen = (name, files, value) => {
		if(files[0]) {
			this.imageFile = files[0]
		}
	}

	_onSwitchChange = (state) => {
		this.setState({
			mode: state ? 'async' : 'sync'
		});
	}

	_beforeImageSearch = (result) => {
		this.setState({
			loaded: false
		});
	}

	_afterImageSearch = (result) => {
		this.setState({
			loaded: true,
			asyncToken: result.asyncToken
		});
	}

	componentDidMount = () => {
		ImageSearchStore.addChangeListener(ImageSearchConstants.BEFORE_SEARCH_IMAGE, this._beforeImageSearch);
        ImageSearchStore.addChangeListener(ImageSearchConstants.AFTER_SEARCH_IMAGE, this._afterImageSearch);
    }

    componentWillMount = () => {
    	ImageSearchStore.removeChangeListener(ImageSearchConstants.BEFORE_SEARCH_IMAGE, this._beforeImageSearch);
        ImageSearchStore.removeChangeListener(ImageSearchConstants.AFTER_SEARCH_IMAGE, this._afterImageSearch);
    }

	imageFile = {}

	state = {
		url: '',
		minHeight: '',
		minWidth: '',
		asyncToken: '',
		mode: 'sync',
		loaded: true
	}
}

reactMixin.onClass(ImageSearchForm, History);

export default ImageSearchForm;