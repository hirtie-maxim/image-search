import React from 'react';
import Loader from 'react-loader';
import JSONTree from 'react-json-viewer';
import _ from 'lodash';

import Img from '../utils/Image.jsx';

import ImageSearchStore from '../stores/ImageSearchStore';

class ImageSearchResult extends React.Component {
	constructor() {
		super();
	}

	render() {
		var { token } = this.props.params,
			images = [];
		_.each(this.state.result.images, (item) => {
			images.push(<Img width="200px" key={item.id} src={item.url} usePlaceholder={false} />);
		});

		return (
			<div className="container">
        		<div className="row">
        			<Loader loaded={this.state.loaded}>
        				{images}
        				<br />
        				<br />
        				<JSONTree json={this.state.result.images} />
        			</Loader>
        		</div>
        	</div>
		);
	}

	componentDidMount = () => {
		ImageSearchStore.getResults(this.props.params.token).then((result) => {
			if(result.success) {
				this.setState({
					result: result.imageSearch,
					loaded: true
				});
			}
		});
	}

	state = {
		loaded: false,
		result: {
			images: {}
		}
	}
}



export default ImageSearchResult;