import React from 'react';
import { Link, History } from 'react-router';
import Formsy from 'formsy-react';
import { File, Input } from 'formsy-react-components';
import uuid from 'node-uuid';
import reactMixin from 'react-mixin';
import UUIDValidator from 'uuid-validate';

Formsy.addValidationRule('isUUID', (values, value) => {
  	return UUIDValidator(value);
});

class HomePage extends React.Component {
	render() {
		return (
			<div id="home-page">
				<Formsy.Form onValidSubmit={this._onSubmit} className="form-horizontal">
					<div className="form-group">
					<Input rowClassName="form-group" className="col-sm-10" placeholder="Asynctoken to get results of search" 
						id="token" name="token" type="text" label="Url" validations="isUUID" 
						value={this.state.token} validationError="This is not a valid token" required />
					</div>
      				<div className="form-group">
						<div className="col-sm-offset-2 col-sm-10">
							<button type="submit" className="btn btn-default">Search</button>
						</div>
					</div>
					<div className="form-group">
						<div className="col-sm-offset-2 col-sm-10">
							<Link to="/search">Place a search</Link>
						</div>
					</div>
				</Formsy.Form>
			</div>
		);
	}

	_onSubmit = (model) => {
		this.context.history.replaceState(null, `/result/${model.token}`);
	}

	state = {
		token: ''
	}
}

reactMixin.onClass(HomePage, History);

export default HomePage;