import React from 'react';
import JSONViewer from 'react-json-viewer';

import ImageSearchStore from '../stores/ImageSearchStore';
import ImageSearchConstants from '../constants/ImageSearchConstants';

class RequestDataShow extends React.Component {
	constructor() {
		super();
	}

	render() {
        return (
        	<div className="container">
        		<div className="row">
        			<h1>Request data</h1>
        			<JSONViewer json={this.state.json} />
        		</div>
        	</div>
        );
    }

    componentDidMount = () => {
		ImageSearchStore.addChangeListener(ImageSearchConstants.BEFORE_SEARCH_IMAGE, this._onChange);
	}

	componentWillMount = () => {
		ImageSearchStore.removeChangeListener(ImageSearchConstants.BEFORE_SEARCH_IMAGE, this._onChange);
	}

	_onChange = (item) => {
		this.setState({
			json: item
		});
	}

    state = {
    	json: {}
    }
}

export default RequestDataShow;