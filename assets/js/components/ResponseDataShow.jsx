import React from 'react';
import JSONTree from 'react-json-viewer';

import ImageSearchStore from '../stores/ImageSearchStore';
import ImageSearchConstants from '../constants/ImageSearchConstants';

class ResponseDataShow extends React.Component {
	constructor() {
		super();
	}

	render() {
        return (
            <div className="container">
                <div className="row">
                    <h1>Response data</h1>
                    <JSONTree json={this.state.json} />
                </div>
            </div>
        );
    }

    componentDidMount = () => {
        ImageSearchStore.addChangeListener(ImageSearchConstants.AFTER_SEARCH_IMAGE, this._onChange);
    }

    componentWillMount = () => {
        ImageSearchStore.removeChangeListener(ImageSearchConstants.AFTER_SEARCH_IMAGE, this._onChange);
    }

    _onChange = (item) => {
        this.setState({
            json: item
        });
    }

    state = {
    	json: {}
    }
}

export default ResponseDataShow;