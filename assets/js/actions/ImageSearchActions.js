import ImageSearchConstants from '../constants/ImageSearchConstants';
import ImageSearchDispatcher from '../dispatchers/ImageSearchDispatcher';

export default class ImageSearchActions {
	static search(imageSearchObject) {
		ImageSearchDispatcher.handleViewAction({
			actionType: ImageSearchConstants.SEARCH_IMAGE,
			item: imageSearchObject
		});
	}

	static getResults(asyncToken) {
		ImageSearchDispatcher.handleViewAction({
			actionType: ImageSearchConstants.GET_RESULTS,
			item: asyncToken
		});
	}
}