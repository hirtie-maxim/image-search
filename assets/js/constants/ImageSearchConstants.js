export default {
	SEARCH_IMAGE: 'search_image',
	BEFORE_SEARCH_IMAGE: 'before_search_image',
	AFTER_SEARCH_IMAGE: 'after_search_image',
	GET_RESULTS: 'get_results'
}