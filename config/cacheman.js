module.exports.cacheman = {
  driver: 'memory',

  memory: {
    engine: 'cacheman-memory'
  },
  
  file: {
    engine: 'file'
  }

}