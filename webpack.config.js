'use strict';

var BowerWebpackPlugin = require('bower-webpack-plugin'),
    path = require('path'),
    webpack = require('webpack'),
    babelSettings = {
        presets: ['es2015', 'stage-0', 'react']
    },
    babelSettingsString = JSON.stringify(babelSettings),
    isProd = process.env.NODE_ENV == 'production',
    entries = ['./assets/js/main.jsx'],
    loaders = [{
        test: /(\.sass|\.scss)$/,
        loader: "style!css!sass?indentedSyntax&outputStyle=expanded&includePaths[]=" + (path.resolve(__dirname, "./node_modules"))
    }, {
        test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&minetype=application/font-woff"
    }, {
        test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&minetype=application/font-woff"
    }, {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&minetype=application/octet-stream"
    }, {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file"
    }, {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: "url?limit=10000&minetype=image/svg+xml"
    }, {
        test: /\.json$/,
        loader: "json"
    }, {
        test: /\.css$/,
        loader: "style!css"
    }],
    plugins = [
        new webpack.NoErrorsPlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        new BowerWebpackPlugin({
            modulesDirectories: ["assets/js/dependencies"],
            manifestFiles: "bower.json",
            includes: /.*/,
            excludes: [],
            searchResolveModulesDirectories: false
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ];

console.log('Is production: ', isProd);

if (!isProd) {
    entries.push('webpack/hot/only-dev-server');
    entries.push('webpack-dev-server/client?http://localhost:11235');
    loaders.push({
        test: /(\.jsx|\.js)$/,
        loader: "eslint-loader",
        exclude: /node_modules/
    });
    loaders.push({
        test: /(\.jsx|\.js)$/,
        loader: 'react-hot!babel?' + babelSettingsString,
        exclude: /node_modules/
    });
    plugins.push(new webpack.HotModuleReplacementPlugin());
} else {
    loaders.push({
        test: /(\.jsx|\.js)$/,
        loader: 'babel?' + babelSettingsString,
        exclude: /node_modules/
    });
}

module.exports = {
    devtool: 'eval',
    entry: entries,
    output: {
        path: path.resolve(__dirname, '.tmp/public/js'),
        filename: 'bundle.js',
        publicPath: 'http://localhost:11235/.tmp/public/js/'
    },
    module: {
        loaders: loaders
    },
    plugins: plugins,
    eslint: {
        configFile: "./.eslintrc"
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    }
};