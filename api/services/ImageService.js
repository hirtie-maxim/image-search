var Promise = require('bluebird'),
    http = require('http'),
    urlService = require('url'),
    sizeOf = require('image-size'),
    tineye = require('tineye'),
    _ = require('lodash'),
    client = new tineye('LCkn,2K7osVwkX95K4Oy', '6mm60lsCNIB,FwOWjJqA80QZHh9BMwc-ber4u=t^'),
    Cache = require('sailsjs-cacheman').sailsCacheman('imageSearchCache');

var ImageService = (function() {
    function ImageService() {}

    ImageService.prototype.search = function(url, options) {
        var self = this;
        return new Promise(function(resolve, reject) {
            self.getImageSize(url).then(function(metadata) {
                client.search(url, function(err, results) {
                    var images = self.filterResults({
                        minWidth: options.minWidth,
                        minHeight: options.minHeight,
                        metadata: metadata
                    }, results.results.matches);

                    self.saveResults({
                        token: options.token,
                        minWidth: options.minWidth,
                        minHeight: options.minHeight,
                        url: url,
                        images: images
                    }).then(function(saved) {
                        resolve(saved);
                    });
                });
            });
        });
    }

    ImageService.prototype.getImageSize = function(url) {
        return new Promise(function(resolve, reject) {
            http.get(urlService.parse(url), function(response) {
                var chunks = [];
                response.on('data', function(chunk) {
                    chunks.push(chunk);
                }).on('end', function() {
                    var buffer = Buffer.concat(chunks);
                    var imageSizes = sizeOf(buffer);
                    resolve({
                        size: imageSizes,
                        length: buffer.length
                    });
                });
            });
        });
    }

    ImageService.prototype.saveResults = function(model) {
        return new Promise(function(resolve, reject) {
            ImageSearch.create({
                token: model.token,
                minWidth: model.minWidth,
                minHeight: model.minHeight,
                url: model.url
            }).then(function(imageSearch) {
                ImageSearch.findOne(imageSearch.id).populate('images').exec(function(err, imageSearch) {
                    _.each(model.images, imageSearch.images.add);
                    imageSearch.save(function(err) {
                        resolve(imageSearch);
                    });
                });
            });
        });
    }

    ImageService.prototype.filterResults = function(options, items) {
        var sizesAreDefined = options.minWidth && options.minHeight,
            filter = function(image) {
                var notDefSizeMatch = !sizesAreDefined && (options.metadata.size.width < image.width && options.metadata.size.height < image.height),
                    defSizeMatch = sizesAreDefined && (options.minWidth < image.width && options.minHeight < image.height),
                    byteMatch = image.size > options.metadata.length;

                return (notDefSizeMatch || defSizeMatch) && byteMatch;
            },
            map = function(match) {
                return {
                    width: match.width,
                    height: match.height,
                    size: match.size,
                    url: match.backlinks[0].url
                };
            };
        return _.filter(_.map(items, map), filter);
    }

    ImageService.prototype.getResults = function(token) {
        return new Promise(function(resolve, reject) {
            Cache.get(token, function(err, value) {
                if (err) {
                    reject(err);
                    return;
                }

                ImageSearch.findOne({
                    token: token
                }).populate('images').then(function(imageSearch) {
                    Cache.set(token, imageSearch, 100, function(err, value) {
                        if (err) {
                            reject(err);
                            return;
                        }

                        console.log(imageSearch);

                        resolve(imageSearch);
                    });
                });
            });
        });
    }

    ImageService.prototype.getCachedImageSearch = function(url) {
        return new Promise(function(resolve, reject) {
            Cache.get(url, function(err, value) {
                if (err) {
                    reject(err);
                }

                if (value) {
                    resolve(value);
                } else {
                    reject(new Error('Not found in cache.'));
                }
            });
        });
    }

    ImageService.prototype.setCachedImageSearch = function(url, value) {
        return new Promise(function(resolve, reject) {
            Cache.set(url, value, 100, function(err, value) {
                if (err) {
                    reject(err);
                    return;
                }

                resolve(value);
            });
        });
    }

    ImageService.prototype.uploadImage = function(file) {
        return new Promise(function(resolve, reject) {
            file.upload(function(err, uploadedFiles) {
                if (err) {
                    reject(err);
                } else {
                    cloudinary.uploader.upload(uploadedFiles[0].fd, function(result) {
                        resolve(result);
                    });
                }
            });
        });
    }

    return ImageService;
})();

module.exports = ImageService;