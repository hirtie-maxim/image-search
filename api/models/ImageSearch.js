/**
* ImageSearch.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	url: 'string',
  	image: 'string',
  	minWidth: 'int',
  	maxWidth: 'int',
  	images: {
  		via: 'imageSearch',
  		collection: 'tinEyeImage'
  	}
  }
};

