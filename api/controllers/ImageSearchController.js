var uuid = require('node-uuid'),
    cloudinary = require('../../cloudinary.js');

module.exports = {
    create: function(req, res) {
        var token = uuid.v4(),
            url = req.param('url'),
            minHeight = req.param('minHeight'),
            minWidth = req.param('minWidth'),
            imageService = new ImageService(),
            isAsync = req.param('mode') === 'async',
            searchAndSave = function(imageUrl) {
                return imageService.search(imageUrl, {
                    minWidth: minWidth,
                    minHeight: minHeight,
                    token: token
                }).then(function(results) {
                    return ImageSearch.findOne(results.id).populate('images').then(function(imageSearch) {
                        if (isAsync) {
                            return;
                        }

                        imageService.setCachedImageSearch(url, imageSearch);
                        return imageSearch;
                    });
                });
            };

        imageService.getCachedImageSearch(url).then(function(value) {
            res.json({
                images: value.images,
                asyncToken: value.token
            });
            res.end();
        }, function() {
            if (isAsync) {
                res.json({
                    asyncToken: token
                });
                res.end();
            }

            if (url) {
                return searchAndSave(url).then(function(imageSearch) {
                    res.json({
                        images: imageSearch.images,
                        asyncToken: imageSearch.token
                    });
                    res.end();
                });
            }

            if (req._fileparser.upstreams.length && req.method == 'POST') {
                return imageService.uploadImage(req.file('image')).then(function(result) {
                    return searchAndSave(result.url).then(function(imageSearch) {
                        res.json({
                            images: imageSearch.images,
                            asyncToken: imageSearch.token
                        });
                        res.end();
                    });
                });
            }
        });


    },
    get: function(req, res) {
        var token = req.param('token'),
            imageService = new ImageService();

        imageService.getResults(token).then(function(imageSearch) {
            res.json({
                success: true,
                imageSearch: imageSearch
            });
            res.end();
        }, function(error) {
            res.json({
                success: false,
                error: error
            });
            res.end();
        });
    }
};